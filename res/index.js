import color from './Colors';
import dimen from './Dimensions';
import string from './String';
import style from './Styles';

export default {
  color,
  dimen,
  string,
  style,
};
