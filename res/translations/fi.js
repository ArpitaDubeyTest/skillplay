const registration = {
  registration: 'Registration',
};

const demo = {
  counter: 'Counter',
};

export default {
  ...registration,
  ...demo,
};
