import dimen from './Dimensions';

const windowWidth = dimen.windowWidth;
const windowHeight = dimen.windowHeight;

const roboto_regular = {
  fontFamily: 'Roboto-Regular',
};

const fontBold = {
  fontFamily: 'AvenirNext-Bold',
};
const fontDemiBold = {
  fontFamily: 'AvenirNext-DemiBold',
};

const fontMedium = {
  fontFamily: 'AvenirNext-Medium',
};

const fontRegular = {
  fontFamily: 'AvenirNext-Regular',
};

const font_56_600 = {
  ...fontDemiBold,
  fontSize: 56,
};

//font-categories
const font_28_500_roboto = {
  ...roboto_regular,
  fontWeight: '500',
  fontSize: 28,
};

const main_heading = {
  ...font_28_500_roboto,
  fontSize: windowHeight > 500 ? 28 : windowHeight / 28,
};

const registration = {
  main_heading,
};

export default {
  ...registration,
};
