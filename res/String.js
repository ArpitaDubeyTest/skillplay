// import I18n from "react-native-i18n";
import en from './translations/en';
import fi from './translations/fi';
import * as RNLocalize from 'react-native-localize';
import I18n from 'i18n-js';
import _ from 'lodash';
// import 'moment/locale/en-ca'
// import 'moment/locale/ca'
I18n.fallbacks = true;

I18n.translations = {
  en,
  fi,
};

export default (key, defaultValue) => {
  return I18n.t(key, {defaultValue});
};

// export const setI18nConfig = () => {
//     const fallback = { languageTag: 'en' }
//     const { languageTag } =
//         RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
//         fallback

//     translate.cache.clear()
//     i18n.translations = { [languageTag]: translationGetters[languageTag]() }
//     i18n.locale = languageTag
// }
