import {Platform, Dimensions, NativeModules} from 'react-native';
// const { StatusBarManager } = NativeModules;

const dimen = {
  windowWidth: Dimensions.get('window').width,
  windowHeight: Dimensions.get('window').height,
  // statusBarHeight: Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT,
  headerHeight: Dimensions.get('window').height / 11,
  footerHeight: Dimensions.get('window').height / 11,
};

export default dimen;
