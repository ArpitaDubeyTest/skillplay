import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import CounterContainer from '../redux/containers/CounterContainer';
const Stack = createStackNavigator();
export default function SkillPlayNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Counter" component={CounterContainer} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
