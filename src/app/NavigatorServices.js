import {NavigationActions} from 'react-navigation';

let _container;

function setContainer(container) {
  _container = container;
}

function navigate(routeName, params) {
  if (!NavigationActions || !NavigationActions.navigate || !routeName) {
    return;
  }

  const navigateAction = NavigationActions.navigate({routeName, params});
  if (_container && _container.dispatch) {
    _container.dispatch(navigateAction);
  }
}

function reset(routeName, params) {
  if (
    !NavigationActions ||
    !NavigationActions.reset ||
    !routeName ||
    !NavigationActions.navigate
  ) {
    return;
  }

  const resetAction = NavigationActions.reset({
    index: 0,
    key: null,
    actions: [NavigationActions.navigate({routeName, params})],
  });

  if (_container && _container.dispatch) {
    _container.dispatch(resetAction);
  }
}

function back(routeKey) {
  if (
    !_container ||
    !routeKey ||
    !NavigationActions ||
    !NavigationActions.back
  ) {
    return;
  }

  _container.dispatch(
    NavigationActions.back({
      key: routeKey,
    }),
  );
}

export default {
  setContainer,
  navigate,
  reset,
  back,
};
