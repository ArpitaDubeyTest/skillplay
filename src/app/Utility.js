export const getTimezone = () => {
  return 'America/Vancouver';
};

export const mergeStateFun = (state, updates) => {
  return state.mergeDeep(updates);
  // return _.merge({}, state, updates);
};
