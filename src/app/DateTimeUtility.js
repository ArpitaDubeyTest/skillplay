import moment from 'moment';
import 'moment-timezone';
import {getTimezone} from './Utility';

let timeZoneVal = undefined;

let GlobalGPSTimestamp = undefined;

export const setGPSTimestamp = (timestamp) => {
  GlobalGPSTimestamp = timestamp;
};

export const getGPSTimestamp = () => {
  return GlobalGPSTimestamp;
};

export const setTimeZone = (tm) => {
  timeZoneVal = tm;
  init();
};

const init = () => {
  if (moment && moment.tz && moment.tz.setDefault) {
    moment.tz.setDefault(
      timeZoneVal ? timeZoneVal : getTimezone && getTimezone(),
    );
  }
};

export const compareDates = (
  firstDate: string,
  secondDate: string,
): boolean => {
  if (!firstDate || !secondDate || !moment || !moment.utc) {
    return undefined;
  }

  init();
  let firstDateMom = moment.utc(firstDate);
  let secondDateMom = moment.utc(secondDate);
  if (
    firstDateMom &&
    secondDateMom &&
    firstDateMom.isBefore &&
    firstDateMom.isBefore(secondDateMom)
  ) {
    return true;
  }
  return false;
};

export const subtractDates = (
  firstDate: string,
  secondDate: string,
): moment.Duration => {
  if (!firstDate || !secondDate || !moment || !moment.utc || !moment.duration) {
    return undefined;
  }
  init();
  let firstDateMom = moment.utc(firstDate);
  let secondDateMom = moment.utc(secondDate);
  let durationObj = undefined;
  if (firstDateMom && secondDateMom && firstDateMom.diff) {
    durationObj = moment.duration(firstDateMom.diff(secondDateMom));
  }
  return durationObj;
};

export const getCurrentTimeStamp = () => {
  if (!moment) {
    return undefined;
  }
  init();
  return moment().toISOString();
};

export const getMoment = (dateTime: string): moment => {
  if (!dateTime || !moment) {
    return undefined;
  }
  init();
  return moment(dateTime);
};

export const formatDate = (dateTime: string, formatStr: string): string => {
  if (!dateTime || !formatStr || !moment) {
    return undefined;
  }
  init();
  return moment(dateTime).format(formatStr);
};

export const subractDateByTimeUnits = (
  dateTime,
  timeUnitName,
  timeUnitValue,
) => {
  if (!dateTime || !timeUnitName || !timeUnitValue || !moment) {
    return undefined;
  }
  init();
  return moment(dateTime)
    .subtract(timeUnitValue, timeUnitName)
    .format('YYYY-MM-DDTHH:mm:ss.000Z');
};

export const dateToUTC = (dateTime: string) => {
  if (!dateTime || !moment) {
    return undefined;
  }
  init();
  return (garageTimeUnformatted = moment
    .utc(moment.utc(dateTime))
    .toDate()
    .toUTCString());
};

export const addSeconds = (seconds) => {
  return moment().add(seconds, 'seconds').format('YYYY-MM-DDTHH:mm:ss.000Z');
};

export const addMinutes = (minutes) => {
  return moment().add(minutes, 'minutes').format('YYYY-MM-DDTHH:mm:ss.000Z');
};

export const getYear = (date) => {
  if (!date) {
    return;
  }
  return moment.utc(date).year();
};
