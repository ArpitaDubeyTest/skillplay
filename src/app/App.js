/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @formats
 * @flow strict-local
 */

import React from 'react';
import {Platform, View, StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import {store} from '../redux/configuration/configureStore';
import SkillPlayNavigator from './SkillPlayNavigator';
// import NavigatorService from './NavigatorServices';

const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <View style={{flex: 1}}>
        <SkillPlayNavigator
        // ref={navigatorRef => NavigatorService && NavigatorService.setContainer ? NavigatorService.setContainer(navigatorRef) : undefined}
        />
        <StatusBar
          translucent={true}
          backgroundColor="rgba(0, 0, 0, 0.1)"
          // background={R.color.grey300}
          barStyle={
            Platform &&
            Platform.select({
              android: 'light-content',
              ios: 'dark-content',
            })
          }
        />
      </View>
    </Provider>
  );
};

export default App;
