class RestHandler {
  constructor() {}
}

//region Singleton
let instance;
export default Object.freeze({
  getInstance() {
    if (!instance) {
      instance = new RestHandler();
    }
    return instance;
  },
});
