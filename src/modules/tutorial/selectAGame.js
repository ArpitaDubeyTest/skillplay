// Imports: Dependencies
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ScrollView,
    Image
} from 'react-native';
import R from '../../../res/index';
import { CheckBox } from 'react-native-elements'

export default class SelectAGame extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView>
                    <View style={styles.imageLogoContainer} >
                        <Image style={styles.imageLogo} source={require('../../../res/images/skillplayLogotransparent2.png')} />
                    </View>
                    <View style={styles.titleHeadingContainer}>
                        <Text data-layer="9f60395d-6f2a-40e9-8ea2-d7b31cc84add" style={styles.titleHeading}>Select a Game</Text>
                    </View>
                    <Text style={styles.tutorialText}>The SkillPlay Platform allows you to Select Games from our broad  
                    <Text>{" "}</Text>
                    <Text style={{ color: 'blue' }}
                        onPress={() => Linking.openURL('')}>
                          Game Portfolio
                    </Text>
                         . Always updated with new games that cater to your likes and wants.</Text>
                </ScrollView>
            </SafeAreaView>
        );
    }
}
// Styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: R.color.black

    },
    imageLogo: {
        width: R.dimen.windowWidth / 4,
        height: R.dimen.windowHeight / 4,
    },
    tutorialText: {
        color: "rgba(201, 201, 201, 1)",
        fontSize: 28,
        marginLeft: 100,
        marginTop: R.dimen.windowHeight / 24,
        fontFamily: 'System',
        fontWeight: '400',
    },
    titleHeadingContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleHeading: {
        backgroundColor: "rgba(255, 255, 255, 0)",
        color: "rgba(226, 130, 8, 1)",
        fontSize: 40,
    },
    imageLogoContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: R.dimen.windowHeight / 9.6
    }
});
