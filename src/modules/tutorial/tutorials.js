
import TutorialIntro from './tutorialIntro';
import SelectAGame from './selectAGame';
import PlaceYourBet from './placeYourBet';
import ChallengePlayers from './challengePlayers';
import PlayGame from './playGame';
import ViewOutCome from './viewOutCome';
import { Pages } from 'react-native-pages';
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ScrollView,
    Image
} from 'react-native';
import R from '../../../res/index';


export default class Tutorials extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <>
                <Pages indicatorPosition="top">
                    <View style={{ flex: 1 }}>
                        <TutorialIntro></TutorialIntro>
                    </View>
                    <View style={{ flex: 1 }}>
                        <SelectAGame></SelectAGame>
                    </View>
                    <View style={{ flex: 1 }}>
                        <PlaceYourBet></PlaceYourBet>
                    </View>
                    <View style={{ flex: 1 }}>
                        <ChallengePlayers></ChallengePlayers>
                    </View>
                    <View style={{ flex: 1 }}>
                        <PlayGame></PlayGame>
                    </View>
                    <View style={{ flex: 1 }}>
                        <ViewOutCome></ViewOutCome>
                    </View>
                </Pages>
            </>

        )
    }
}
