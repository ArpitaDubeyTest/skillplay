// Imports: Dependencies
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ScrollView,
    Image
} from 'react-native';
import R from '../../../res/index';
import { CheckBox } from 'react-native-elements'

export default class TutorialIntro extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView>
                    <View style={styles.imageLogoContainer} >
                        <Image style={styles.imageLogo} source={require('../../../res/images/skillplayLogotransparent2.png')} />
                    </View>
                    <Text data-layer="9f60395d-6f2a-40e9-8ea2-d7b31cc84add" style={styles.fiveStepsToWin}>Five Steps to Win</Text>
                    <Text style={styles.tutorialText}>1.  Select a Game</Text>
                    <Text style={styles.tutorialText}>2.  Pay your Entry Fee</Text>
                    <Text style={styles.tutorialText}>3.  Challenge Players</Text>
                    <Text style={styles.tutorialText}>4.  Play Game</Text>
                    <Text style={styles.tutorialText}>5.  View Outcome</Text>
                    {/* <CheckBox
                        style={{ backgroundColor: R.color.black, color: "rgba(38, 153, 251, 1)", fontSize: 20, width: 5 }}
                        title='do not show intro again'
                    /> */}

                    {/* <CheckBox
                        containerStyle={{ backgroundColor: R.color.black, borderColor: R.color.black, color: "rgba(201, 201, 201, 1)", width: 10 }}>
                    </CheckBox> */}

                    <View style={{ flexDirection: 'column' }}>
                        <View style={{ flexDirection: 'row', }}>
                            <CheckBox
                                value={this.state.checked}
                                onValueChange={() => this.setState({ checked: !this.state.checked })}
                            />
                            <Text style={{marginTop: 15, color: R.color.blue,fontSize: 25, }}>
                                do not show intro again</Text>                           
                        </View>
                    </View>

                </ScrollView>
            </SafeAreaView>
        );
    }
}
// Styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: R.color.black,
        //fontFamily:R.dimen.roboto_regular
    },
    imageLogo: {
        width: R.dimen.windowWidth / 4,
        height: R.dimen.windowHeight /4,
    },
    tutorialText: {
        color: "rgba(201, 201, 201, 1)",
        fontSize: 28,
        marginLeft: 20,
        marginTop: R.dimen.windowHeight / 24,
        fontFamily: 'System',
        fontWeight: '400',
    },
    fiveStepsToWin: {
        backgroundColor: "rgba(255, 255, 255, 0)",
        color: "rgba(226, 130, 8, 1)",
        fontSize: 40,
    },
    imageLogoContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: R.dimen.windowHeight / 9.6
    }
});
