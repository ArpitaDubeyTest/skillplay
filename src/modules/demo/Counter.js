// Imports: Dependencies
import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import R from '../../../res/index';

export default class Counter extends Component {
  constructor(props) {
    super(props);
    this.handleIncrementButton = this.handleIncrementButton.bind(this);
    this.handleDecrementButton = this.handleDecrementButton.bind(this);
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.counterTitle}>{R.string('counter')}</Text>
        <View style={styles.counterContainer}>
          <TouchableOpacity onPress={this.handleIncrementButton}>
            <Text style={styles.buttonText}>{'+'}</Text>
          </TouchableOpacity>
          <Text style={styles.counterText}>{this.props.counter}</Text>
          <TouchableOpacity onPress={this.handleDecrementButton}>
            <Text style={styles.buttonText}>{'-'}</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }

  handleIncrementButton() {
    this.props.actions.reduxIncreaseCounter(this.props.counter);
  }

  handleDecrementButton() {
    this.props.actions.reduxDecreaseCounter(this.props.counter);
  }
}
// Styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  counterContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  counterTitle: {
    fontFamily: 'System',
    fontSize: 32,
    fontWeight: '700',
    color: '#000',
  },
  counterText: {
    fontFamily: 'System',
    fontSize: 36,
    fontWeight: '400',
    color: '#000',
  },
  buttonText: {
    fontFamily: 'System',
    fontSize: 50,
    fontWeight: '300',
    color: '#007AFF',
    marginLeft: 40,
    marginRight: 40,
  },
});
