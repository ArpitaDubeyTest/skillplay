// Imports: Dependencies
import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Image,
  ScrollView
} from 'react-native';
import R from '../../../res/index';

export default class LoadScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
        <View style={styles.logoImageContainer}>
          <Image style={styles.logoImage} source={require('../../../res/images/skillplayLogotransparent2.png')}></Image>
          </View>
          <View style={styles.gifImageContainer}>
          <Image style={styles.gifImage}  source={require('../../../res/images/ZZ5H.gif')}></Image>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
// Styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#000",
  },
  logoImage: {
    width: R.dimen.windowWidth / 3,
    height: R.dimen.windowHeight / 3,
    marginTop:50
  },
  logoImageContainer: {
    width: R.dimen.windowWidth,
    justifyContent: 'center',
    alignItems: 'center',    
  },
  gifImageContainer: {
    width: R.dimen.windowWidth,
    justifyContent: 'center',
    alignItems: 'center',    
  },
  gifImage: {
    width: 50,
    height: 50,
    marginTop:50 
  },
  counterContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
