import {createSelector} from 'reselect';

const getCounterState = (state) => {
  if (!state || !state.counter) {
    return undefined;
  }
  return state.counter;
};

const getCounterData = (state) => {
  const counterState = getCounterState(state);
  if (!counterState) {
    return undefined;
  }
  const counter = counterState.getIn(['counter']);
  return counter;
};

export const getCounter = createSelector(getCounterData, (counter) => {
  if (counter === undefined || counter === '') {
    return 0;
  }
  return counter;
});
