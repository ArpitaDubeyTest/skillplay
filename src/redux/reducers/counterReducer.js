import types from '../actions/ActionTypes';
import initialState from './IntialState';
import {mergeStateFun} from '../../app/Utility';
const counterReducer = (state = initialState.counter, action) => {
  const {type, payload} = action;

  switch (type) {
    case types.INCREASE_COUNTER: {
      if (!payload) {
        return state;
      }
      let counter = payload.counter + payload.value;
      return mergeStateFun(state, {counter: counter});
    }
    case types.DECREASE_COUNTER: {
      if (!payload) {
        return state;
      }
      let counterVal = payload.counter - payload.value;
      return mergeStateFun(state, {counter: counterVal});
    }
    default: {
      return state;
    }
  }
};
export default counterReducer;
