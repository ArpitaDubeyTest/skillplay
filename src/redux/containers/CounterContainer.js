import Counter from '../../modules/demo/Counter';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  reduxIncreaseCounter,
  reduxDecreaseCounter,
} from '../actions/CounterActions';
import {getCounter} from '../selectors/CounterSelectors';
const mapStateToProps = (state) => {
  console.log('CounterContainer Store:', {...state});
  return {
    counter: getCounter(state),
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(
      {
        reduxDecreaseCounter,
        reduxIncreaseCounter,
      },
      dispatch,
    ),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
