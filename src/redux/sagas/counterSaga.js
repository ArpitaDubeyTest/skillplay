import {delay, takeEvery, takeLatest, put} from 'redux-saga/effects';
import types from '../actions/ActionTypes';
import originalData from '../../data/payload.json'
import { article } from '../../network/normalizrSchemas/demo';
import { normalize } from 'normalizr';

function* increaseCounterAsync(action) {
  try {
    console.log('increaseCounterAsync saga action', action);
  } catch (error) {
    console.log(error);
  }
}
export function* watchIncreaseCounter() {
  yield takeLatest(types.INCREASE_COUNTER, increaseCounterAsync);
}
function* decreaseCounter(action) {
  try {
    console.log('decreaseCounter saga action', action);
    const normalizedData = normalize(originalData, article);
    console.log('decreaseCounter saga normalizedData', normalizedData);
  } catch (error) {
    console.log(error);
  }
}
export function* watchDecreaseCounter() {
  yield takeLatest(types.DECREASE_COUNTER, decreaseCounter);
}
