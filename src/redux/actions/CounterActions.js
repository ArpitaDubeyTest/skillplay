import types from './ActionTypes';

export const reduxIncreaseCounter = (counter) => ({
  type: types.INCREASE_COUNTER,
  payload: {value: 1, counter},
});

export const reduxDecreaseCounter = (counter) => ({
  type: types.DECREASE_COUNTER,
  payload: {value: 1, counter},
});
